const functions = require("firebase-functions");
const admin = require("firebase-admin");

// Fetch the service account key JSON file contents
var serviceAccount = require("./firebaseAcc.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://makro-api.firebaseio.com"
});

module.exports = {
  ...require("./makro-api-prd.js"),
  ...require("./makro-customer-app.js"),
};
