const functions = require('firebase-functions');

var admin = require('firebase-admin');

const express = require('express');
const cors = require('cors');
const _ = require('underscore');


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));


// shoplist
app.get('/customers/shopList/:documentName/:documentValue', (req, res) => {
    let params = ['documentName', 'documentValue', 'shopListId', 'contactUUID', 'emailAddress', 'country', 'favoriteLocationID', 'status', 'dateTimeCreation', 'dateTimeLastUpdate'];
    let documentValue = req.params.documentValue;
    let query = {};
    query.documentName = req.params.documentName;
    let i = params.forEach(item => {
        if (req.query[item]) {
            query[item] = req.query[item];
        }
    });

    if (!query.documentName || !documentValue) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.' });
    } else {
        admin.database().ref('/shoplists').orderByChild('documentValue').equalTo(documentValue).once('value').then((snapshot) => {
            // console.log(snapshot.val())

            if (snapshot.val() && Object.keys(snapshot.val()).length > 0) {
                let values = snapshot.val();
                values = Object.keys(values).map(item => {
                    values[item].shopListId = item;
                    return values[item];
                })
                if (query && Object.keys(query).length > 0) {
                    let arr = _.where(values, query);
                    if (arr && arr.length > 0) {
                        return res.send(200, arr);
                    } else {
                        return res.send(400, { code: 400, message: 'Invalid Identifier.' });
                    }
                } else {
                    return res.send(200, values);
                }
            } else {
                return res.send(400, { code: 400, message: 'Invalid Identifier.' });
            }
        })
            .catch(err => {
                return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
            });
    }

});


app.post('/customers/shopList', (req, res) => {
    if (!req.body || !req.body.documentName || !req.body.documentValue) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing fields.' });
    } else if (req.body.shopListId || req.body.id) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Field ShopListId is not allowed.' });
    } else {
        req.body.dateTimeCreation = new Date().toISOString();
        admin.database().ref('/shoplists').push(req.body).then((snap) => {
            let idA = snap.toString().split('/');
            let i = (idA.length > 0) ? idA[idA.length - 1] : 0;
            return res.send(201, { ShopListId: i });
        }).catch(err => {
            return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
        });
    }

});

app.put('/customers/shopList/:shopListId', (req, res) => {
    let shopListId = req.params.shopListId;
    if (!req.body || !req.body.documentName || !req.body.documentValue) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing fields.' });
    } else if (req.body.shopListId || req.body.id) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Field ShopListId is not allowed.' });
    } else {
        req.body.dateTimeLastUpdate = new Date().toISOString();
        admin.database().ref('/shoplists/' + shopListId).once('value').then((snapshot) => {
            if (snapshot.val()) {
                req.body.dateTimeCreation = snapshot.val().dateTimeCreation;
                return admin.database().ref('/shoplists/' + shopListId).set(req.body).then(() => {
                    return res.send(200);
                }).catch(err => {
                    return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
                });
            } else {
                return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'shopListItemId Not Found.' });
            }

        }).catch(err => {
            return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
        });
    }

});

// shoplistitems
app.get('/customers/shopListItems/:shopListId', (req, res) => {
    let params = ['shopListId', 'productSKU', 'status', 'dateTimeCreation', 'dateTimeLastUpdate'];
    let shopListId = req.params.shopListId;
    let query = {};
    let i = params.forEach(item => {
        if (req.query[item]) {
            query[item] = req.query[item];
        }
    });
    if (!shopListId) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.' });
    } else {
        admin.database().ref('/shoplistitems').orderByChild('shopListId').equalTo(shopListId)
            .once('value').then((snapshot) => {
                if (snapshot.val() && Object.keys(snapshot.val()).length > 0) {
                    let values = snapshot.val();
                    values = Object.keys(values).map(item => {
                        values[item].shopListItemId = item;
                        return values[item];
                    })
                    if (query && Object.keys(query).length > 0) {
                        let arr = _.where(values, query);
                        if (arr && arr.length > 0) {
                            return res.send(200, arr);
                        } else {
                            return res.send(400, { code: 400, message: 'Invalid Identifier.' });
                        }
                    } else {
                        return res.send(200, values);
                    }
                } else {
                    return res.send(400, { code: 400, message: 'Invalid Identifier.' });
                }
            })
            .catch(err => {
                return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
            });
    }

});

app.post('/customers/shopListItems', (req, res) => {

    if (!req.body) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.' });
    } else if (req.body.shopListItemId || req.body.id) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Field ShopListItemId is not allowed.' });
    } else if (!req.body.shopListId) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing foreign key shopListId.' });
    } else {
        admin.database().ref('/shoplists/').child(req.body.shopListId)
            .once("value").then((snap) => {
                if (snap.val()) {
                    req.body.dateTimeCreation = new Date().toISOString();
                    return admin.database().ref('/shoplistitems').push(req.body).then((snap) => {
                        var idA = snap.toString().split('/');
                        let i = (idA.length > 0) ? idA[idA.length - 1] : 0;
                        return res.send(201, { ShopListItemId: i });
                    }).catch(err => {
                        return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
                    });
                } else {
                    return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing foreign key shopListId.' });
                }
            }).catch(err => {
                return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
            });



    }

});

app.put('/customers/shopListItems/:shopListItemId', (req, res) => {
    let shopListItemId = req.params.shopListItemId;
    if (!req.body) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.' });
    } else if (req.body.shopListItemId || req.body.id) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Field ShopListItemId is not allowed.' });
    } else if (!req.body.shopListId) {
        return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing foreign key shopListId.' });
    } else {
        admin.database().ref('/shoplists').child(req.body.shopListId)
            .once("value").then((snap) => {
                if (snap.val()) {
                    req.body.dateTimeLastUpdate = new Date().toISOString();
                    return admin.database().ref('/shoplistitems/' + shopListItemId).once('value').then((snapshot) => {
                        if (snapshot.val()) {
                            req.body.dateTimeCreation = snapshot.val().dateTimeCreation;
                            return admin.database().ref('/shoplistitems/' + shopListItemId).set(req.body).then(() => {
                                return res.send(200);
                            }).catch(err => {
                                return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
                            });
                        } else {
                            return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'shopListItemId Not Found.' });
                        }
                    }).catch(err => {
                        return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
                    });
                } else {
                    return res.send(400, { code: 400, message: 'Invalid Identifier.', target: 'Missing foreign key shopListId.' });
                }
            }).catch(err => {
                return res.send(500, { code: 500, message: 'Something wrong has happened.', target: err });
            });


    }

});



app.use(function (req, res, next) {
    res.status(404);

    res.render('404', { code: 404, message: 'Not Found.' });
});

// Expose Express API as a single Cloud Function:
exports.api = functions.https.onRequest(app);
