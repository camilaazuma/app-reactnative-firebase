'use strict';

const cors = require('cors')({origin: true});
const functions = require('firebase-functions');
const request = require('request-promise');
const htmlparser = require('node-html-parser');

const BASE_URL = "https://makrogroup.apimanagement.us2.hana.ondemand.com/v1/";

exports.getCustomerDocuments = functions.https.onRequest((req, response) => {
    cors(req, response, function(){
      const params = typeof req.body === "string" ? JSON.parse(req.body) : req.body;
      const token = params.token;
      const countryId = params.countryId;
      const documentName = params.documentName;
      const documentValue = params.documentValue;

      request({
          uri: BASE_URL + "customers/" + countryId + "/documents/" + documentName + "/" + documentValue,
          method: 'GET',
          headers: {
            'Authorization': token
          },
          json: true,
          resolveWithFullResponse: true,
      })
      .then(function(r){
          response.send(r.body);
          return r;
      })
      .catch((e) => response.status(e.statusCode).send(e.message));
    });
});

exports.getCustomerRecommendations = functions.https.onRequest((req, response) => {
    cors(req, response, function(){
      const params = typeof req.body === "string" ? JSON.parse(req.body) : req.body;
      const token = params.token;
      const countryId = params.countryId;
      const contactUUID = params.contactUUID;
      const scenarioID = params.scenarioID;

      request({
          uri: BASE_URL + "customers/" + countryId + "/" + contactUUID + "/recommendations/" + scenarioID,
          method: 'GET',
          headers: {
            'Authorization': token
          },
          json: true,
          resolveWithFullResponse: true,
      })
      .then(function(r){
        response.send(r.body);
        return r;
      })
      .catch((e) => response.status(e.statusCode).send(e.message));
    });
});

exports.getProduct = functions.https.onRequest((req, response) => {
    cors(req, response, function(){
      const params = typeof req.body === "string" ? JSON.parse(req.body) : req.body;

      const token = params.token;
      const countryId = params.countryId;
      const storeId = params.storeId;
      if(params.barcode){
        request({
            uri: BASE_URL + "products/" + countryId + "/location/" + storeId + "/barcode/" + params.barcode,
            method: 'GET',
            headers: {
              'Authorization': token
            },
            json: true,
            resolveWithFullResponse: true,
        })
        .then(function(r){
          response.send(r.body);
          return r;
        })
        .catch((e) => response.status(e.statusCode).send(e.message));
      }else if(params.sku) {
        request({
            uri: BASE_URL + "products/" + countryId + "/location/" + storeId + "/sku/" + params.sku,
            method: 'GET',
            headers: {
              'Authorization': token
            },
            json: true,
            resolveWithFullResponse: true,
        })
        .then(function(r){
          response.send(r.body);
          return r;
        })
        .catch((e) => response.status(e.statusCode).send(e.message));
      }else if(params.name){
        response.status(404).send("Not implemented yet");
      }else{
        response.status(406).send("Invalid request");
      }
    });
});

exports.getStoreOffers = functions.https.onRequest((req, response) => {
    cors(req, response, function(){
      const params = typeof req.body === "string" ? JSON.parse(req.body) : req.body;

      const token = params.token;
      const countryId = params.countryId;
      const storeId = params.storeId;

      request({
          uri: BASE_URL + "locations/" + countryId + "/" + storeId + "/offers",
          method: 'GET',
          headers: {
            'Authorization': token
          },
          json: true,
          gzip: true,
          resolveWithFullResponse: true,
      })
      .then(function(r){
        let urls = r.body.StoreOffers.map(item => BASE_URL + "products/" + countryId + "/" + item.ProductSKU);

        // consulta infos dos produtos 1 a 1
        const promises = urls.map(async (url) => request({uri: url, method: 'GET', headers: {'Authorization': token}, json: true})
        .then(function(success){return success}, function(error){return error.statusCode}));

        // espera tudo resolver e verifica se tem as informações necessárias
        Promise.all(promises).then(values => {
          r.body.StoreOffers.map(offer => {
            if(offer.ProductSKU){
              var prod = values.filter(product => product.ProductSKU === offer.ProductSKU);
              if(prod.length){
                offer.ProductName = prod[0].ProductName ? prod[0].ProductName : null;
                offer.ProductSalesPomoPackingDescription = prod[0].ProductSalesPomoPackingDescription ? prod[0].ProductSalesPomoPackingDescription : null;
                offer.ProductImages = prod[0].ProductImages? prod[0].ProductImages : null;
                return offer;
              }
            }
            return null;
          })
          response.send(r.body);
          return r;
        });
      })
      .catch((e) => response.status(e.statusCode).send(e.message));

    });
});
